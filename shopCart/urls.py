from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf.urls.static import static
from django.conf import settings
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'shopCart.views.home', name='home'),
    url(r'^$', 'shopicart.views.login_view', name='login_view'),
    url(r'^shopicart/',include('shopicart.urls',namespace="shopicart")),
    url(r'^admin/', include(admin.site.urls)),
)+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)	

urlpatterns += staticfiles_urlpatterns()