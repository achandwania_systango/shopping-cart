"""
Django settings for shopCart project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))
TEMPLATE_DIRS = (os.path.join(BASE_DIR,'templates'),)
# To write your own admin view or make changes in admin view.

# Quick-start development settings   - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'ww@rwgz30$$7*tdb4od6p&*6&+*+b7!wx#)8p&_qro^e^d0de#'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'shopicart',
    'sorl.thumbnail',
    'mathfilters',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'shopCart.urls'

WSGI_APPLICATION = 'shopCart.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME'  : 'shopcart',
        'USER'  : 'root',
        'PASSWORD': 'root',

    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/


#STATIC_ROOT = "/home/achandwania/shopCart/static/"
STATIC_URL = '/static/'
MEDIA_ROOT = "/home/achandwania/shopCart/shopicart/media/"
MEDIA_URL =  "media/"
LOGIN_URL =  "/shopicart/logincheck/"

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, "static"),
    '/home/achandwania/shopCart/static/',
)

TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
)

import sys
for a in INSTALLED_APPS:
   try:
       sys.path.append(__import__(a + '.templatetags', {}, {}, ['']).__path__)
   except ImportError:
       pass