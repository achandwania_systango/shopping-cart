from django.db import models
from sorl.thumbnail import ImageField, get_thumbnail
from PIL import Image, ImageOps
from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User
from datetime import datetime

class Category(models.Model):
  category = models.CharField(max_length=100,unique=True) 
  def __str__(self):
    return self.category

class Products(models.Model):
  title = models.CharField(max_length=100)
  description = models.CharField(max_length=300)
  price = models.DecimalField(max_digits=15,decimal_places=2)
  Category = models.ForeignKey(Category) 
  image = models.ImageField(upload_to="images/",blank=False, null=True,editable=True)
  add_date = models.DateTimeField(auto_now_add=True)
  modified_date = models.DateTimeField(auto_now=True)

  def __str__(self):
    return self.title

  def slide_thumbnail(self):
    if self.image:
      return '<img src="/%s" />' % self.image.url
  slide_thumbnail.short_description = 'Image'
  slide_thumbnail.allow_tags = True

class UserCart(models.Model):
  user = models.ForeignKey(User,null=True)  
  product = models.ForeignKey(Products)
  add_date = models.DateField(auto_now_add=True,default=datetime.now())
  qty = models.IntegerField(max_length=2,default=1)


class History(models.Model):
  pur_date = models.DateField(auto_now_add=True,default=datetime.now())
  user = models.ForeignKey(User,null=True)
  title = models.CharField(max_length=100,null=True)
  price = models.DecimalField(max_digits=15,decimal_places=2)
  image = models.ImageField(upload_to="images/",blank=False, null=True,editable=True)
  qty = models.IntegerField(max_length=2,default=1)
  def __str__(self):
    return self.title