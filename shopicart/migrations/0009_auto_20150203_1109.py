# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('shopicart', '0008_auto_20150203_1022'),
    ]

    operations = [
        migrations.AddField(
            model_name='usercart',
            name='add_date',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 3, 11, 9, 9, 932388), auto_now_add=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='history',
            name='pur_date',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 3, 11, 9, 9, 932904), auto_now_add=True),
            preserve_default=True,
        ),
    ]
