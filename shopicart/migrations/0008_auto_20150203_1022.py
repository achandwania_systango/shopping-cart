# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('shopicart', '0007_auto_20150203_1009'),
    ]

    operations = [
        migrations.AlterField(
            model_name='history',
            name='pur_date',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 3, 10, 22, 3, 827770), auto_now_add=True),
            preserve_default=True,
        ),
    ]
