# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('shopicart', '0004_auto_20150203_0849'),
    ]

    operations = [
        migrations.AddField(
            model_name='history',
            name='pur_date',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 3, 8, 50, 54, 750562), auto_now_add=True),
            preserve_default=True,
        ),
    ]
