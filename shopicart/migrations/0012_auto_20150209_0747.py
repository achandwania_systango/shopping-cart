# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('shopicart', '0011_auto_20150205_1418'),
    ]

    operations = [
        migrations.AddField(
            model_name='history',
            name='qty',
            field=models.IntegerField(default=1, max_length=2),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='history',
            name='pur_date',
            field=models.DateField(default=datetime.datetime(2015, 2, 9, 7, 47, 12, 191621), auto_now_add=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='usercart',
            name='add_date',
            field=models.DateField(default=datetime.datetime(2015, 2, 9, 7, 47, 12, 191120), auto_now_add=True),
            preserve_default=True,
        ),
    ]
