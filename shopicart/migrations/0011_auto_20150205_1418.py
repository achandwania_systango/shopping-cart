# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('shopicart', '0010_auto_20150205_1414'),
    ]

    operations = [
        migrations.AlterField(
            model_name='history',
            name='pur_date',
            field=models.DateField(default=datetime.datetime(2015, 2, 5, 14, 18, 3, 752432), auto_now_add=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='usercart',
            name='add_date',
            field=models.DateField(default=datetime.datetime(2015, 2, 5, 14, 18, 3, 751920), auto_now_add=True),
            preserve_default=True,
        ),
    ]
