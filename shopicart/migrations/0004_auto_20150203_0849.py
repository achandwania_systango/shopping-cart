# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('shopicart', '0003_auto_20150203_0838'),
    ]

    operations = [
        migrations.AlterField(
            model_name='history',
            name='price',
            field=models.DecimalField(max_digits=15, decimal_places=2),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='products',
            name='price',
            field=models.DecimalField(max_digits=15, decimal_places=2),
            preserve_default=True,
        ),
    ]
