# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('shopicart', '0012_auto_20150209_0747'),
    ]

    operations = [
        migrations.AddField(
            model_name='usercart',
            name='qty',
            field=models.IntegerField(default=1, max_length=2),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='history',
            name='pur_date',
            field=models.DateField(default=datetime.datetime(2015, 2, 9, 8, 22, 21, 597038), auto_now_add=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='usercart',
            name='add_date',
            field=models.DateField(default=datetime.datetime(2015, 2, 9, 8, 22, 21, 596485), auto_now_add=True),
            preserve_default=True,
        ),
    ]
