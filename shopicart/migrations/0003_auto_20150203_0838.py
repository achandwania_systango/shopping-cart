# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('shopicart', '0002_auto_20150203_0800'),
    ]

    operations = [
        migrations.AlterField(
            model_name='products',
            name='add_date',
            field=models.DateTimeField(auto_now_add=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='products',
            name='modified_date',
            field=models.DateTimeField(auto_now=True),
            preserve_default=True,
        ),
    ]
