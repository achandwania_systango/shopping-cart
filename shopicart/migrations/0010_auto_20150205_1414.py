# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('shopicart', '0009_auto_20150203_1109'),
    ]

    operations = [
        migrations.AlterField(
            model_name='history',
            name='pur_date',
            field=models.DateField(default=datetime.datetime(2015, 2, 5, 14, 14, 43, 91903), auto_now_add=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='usercart',
            name='add_date',
            field=models.DateField(default=datetime.datetime(2015, 2, 5, 14, 14, 43, 91395), auto_now_add=True),
            preserve_default=True,
        ),
    ]
