from django.conf.urls import patterns, url

from shopicart import views
 #url(r'^(?P<question_id>\d+)/vote/$', views.vote, name='vote'),
urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
    url(r'^register/$', views.register, name='register'),
    url(r'^home/$', views.home, name='home'),
    url(r'^logincheck/$', views.logincheck, name='logincheck'),   
    url(r'^logout/$', views.logout_view, name='logout_view'),
   # url(r'^home/category/(?\D)+/$', views.category, name='category'),
    url(r'^home/category/(?P<category_id>\d+)/$', views.category, name='category'),
    url(r'^home/userCart/(?P<user_id>\d+)/$', views.mycart, name='mycart'),
   # url(r'^home/userCart/$', views.mycartblank, name='mycartblank'),
    url(r'^home/addcart/(?P<product_id>\d+)/$', views.addcart, name='addcart'),
    url(r'^home/userCart/(?P<user_id>\d+)/removecart/(?P<product_id>\d+)/$', views.removecart, name='removecart'),
    url(r'^home/userCart/(?P<user_id>\d+)/checkout/$', views.checkout, name='checkout'),
    url(r'^home/userCart/(?P<user_id>\d+)/payamount/$', views.payamount, name='payamount'),
    url(r'^home/userCart/(?P<user_id>\d+)/history/$', views.history, name='history'),
   # url(r'^home/userCart/(?P<user_id>\d+)/history/removehistory/(?P<product_id>\d+)/$', views.removehistory, name='removehistory'),
   # url(r'^home/userCart/(?P<user_id>\d+)/history/removehistoryall/$', views.removehistoryall, name='removehistoryall'),
    url(r'^home/search/$', views.search, name='search'),
    url(r'^home/userCart/(?P<user_id>\d+)/history/(?P<year>\d+)/(?P<month>\d+)/(?P<day>\d+)/historydel/$', views.historydel, name='historydel'),
    url(r'^myexp/$',views.myexp,name='myexp')
    )
