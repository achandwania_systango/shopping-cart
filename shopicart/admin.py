from django.contrib import admin
from shopicart.models import Products,Category, History
from sorl.thumbnail.admin import AdminImageMixin
from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User

# Register your models here.
class productAdmin(admin.ModelAdmin):

  list_display = ('title','slide_thumbnail','price','Category','description','add_date','modified_date') #mail list display
  #fields = ['title','description','price','image'] #on click edit
  search_fields = ['title']
  list_filter = ['Category']

class categoryAdmin(admin.ModelAdmin):
  fields = ['category']

admin.site.register(Products,productAdmin)
admin.site.register(Category,categoryAdmin)