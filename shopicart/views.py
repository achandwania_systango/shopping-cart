from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect,HttpResponse
from shopicart.models import Products, UserCart, Category, History
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from datetime import datetime

# Create your views here.

def myexp(request):
  values = {'Title':'mytitle','userid':'2','username':'Jhon'}
  context = {'content':values}
  return render(request,'shopicart/child.html',context)

def index(request):
  context = {}
  return render(request, 'shopicart/index.html',context)

def login_view(request):
  context = {}
  return render(request,'shopicart/index.html',context)

def logincheck(request):
  if request.method == 'POST':
    uid = request.POST.get('id')
    upass = request.POST.get('password')
    user = authenticate(username=uid,password=upass)
    if user is not None:   #for pass is not valid
      login(request,user)
      request.session['username'] = user.first_name
      request.session['userid'] = user.id
      return  HttpResponseRedirect('/shopicart/home/')
    else:
      context = {'message' : 'Username and password is not matched.. '}
      return render(request, 'shopicart/index.html',context)
  else:
    context = {}
    return render(request,'shopicart/index.html',context)


def register(request):
  context = {}
  if request.method == 'POST':
    try:
      context = {}
      uname= request.POST.get('uid')
      uemail= request.POST.get('id')
      upass = request.POST.get('password')
      user = User.objects.create_user(uemail,uemail,upass)
      user.first_name = uname
      user.save()
      user = authenticate(username=uemail,password=upass)
      login(request,user)
      request.session['username'] = user.first_name
      request.session['userid'] = user.id
      print request.session.get('userid')
      return HttpResponseRedirect('/shopicart/home/')
    except Exception, e:
      context = {'message':'Mail id already exist'}
      return render(request,'shopicart/register.html',context)
  else:
    return render(request, 'shopicart/register.html',context)

@login_required
def home(request):
  #if request.user.is_authenticated():
    products = Products.objects.all()
    category = Category.objects.all()
    context = {'userid':request.session.get('userid'),'media':'/media/','product': products,'username': request.session.get('username'),'products': products,'category':category}
    if request.GET.get('msg'):
      msg = request.GET.get('msg')
      context['message'] = msg
    if request.GET.get('search'): 
      search = request.GET.get('search')
      products = Products.objects.filter(title__icontains=search)
      context['products'] = products
    return render(request, 'shopicart/homec.html',context)

@login_required
def logout_view(request): 
  context = {}
  request.session['logon'] = 'false'
  request.session['username'] = 'None'
  request.session['userid'] = '0'
  logout(request)
  return render(request,'shopicart/logout.html',context)

@login_required
def category(request,category_id):
  context = {'username':request.session.get('username')}
  products = Products.objects.filter(Category_id=category_id)
  category = Category.objects.all()
  context = {'media':'/media/','username': request.session.get('username'),'products': products,'category':category,'userid':request.session.get('userid')}
  return render(request,'shopicart/home.html',context)

@login_required
def mycart(request,user_id):
  poList = []
  productid = UserCart.objects.filter(user_id=int(user_id))  
  for pid in productid:
    poList.append(Products.objects.get(pk=pid.product_id))
  if not poList:
    msg = 'Cart is empty'
  elif request.GET.get('msg'):
    msg = request.GET.get('msg')
  else:
    msg = ''
  context = {'ucobj':productid,'message':msg,'products':poList,'media':'/media/','username':request.session.get('username'),'userid':request.session.get('userid')}
  return render(request,'shopicart/mycartc.html',context)

@login_required
def addcart(request,product_id):
  objc = UserCart.objects.filter(user_id=request.session.get('userid'))
  notFound = 'True'
  for obj in objc:
    if obj.product_id == int(product_id):
      obj.qty = obj.qty + 1
      obj.save()
      notFound = ''
      break
  if notFound:
    objuc = UserCart(user_id=request.session.get('userid'),product_id=product_id)
    objuc.save()
  obj= Products.objects.get(id=product_id)
  context = {'message':obj.title+' add to cart successfully'}
  return HttpResponseRedirect('/shopicart/home/?msg='+obj.title+' add to cart successfully')
    #return HttpResponseRedirect('/shopicart/home/')
    #return render(request,'shopicart/redirect.html',context)

@login_required
def removecart(request,user_id,product_id):
  productid = UserCart.objects.filter(user_id=int(user_id))
  for pid in productid:
    if pid.product_id == int(product_id):
      if pid.qty > 1:
        pid.qty = pid.qty - 1
        pid.save()
      else:
        pid.delete()
      break
  obj = Products.objects.get(id=product_id)
  message = obj.title+' remove from cart'
  #return HttpResponseRedirect(reverse('shopicart:mycart',kwargs=context))
  return HttpResponseRedirect('/shopicart/home/userCart/'+user_id+'/?msg='+message)
  #return render(request,'shopicart/removecart.html',context)

@login_required
def checkout(request,user_id):
  total = 0
  poList = []
  pList = []
  msg = ''
  productid = UserCart.objects.filter(user_id=int(user_id))  
  for pid in productid:
    obj = Products.objects.get(pk=pid.product_id)
    total = total + (obj.price*pid.qty)
    poList.append(Products.objects.get(pk=pid.product_id)) 
  if not poList:
    msg = 'No products in checkout'
  context = {'ucobj':productid,'message':msg,'total':total,'products':poList,'media':'/media/','username':request.session.get('username'),'userid':request.session.get('userid')}
  return render(request,'shopicart/checkoutc.html',context)

@login_required
def payamount(request,user_id):
  productid = UserCart.objects.filter(user_id=int(user_id))
  for upid in productid:
    pid = Products.objects.get(id=upid.product_id)
    obj = History(qty=upid.qty,pur_date=datetime.now(),user_id=user_id,title=pid.title,price=pid.price*upid.qty,image=pid.image)
    obj.save()
    upid.delete()
  #context = {'msg':'Products purchased successfully'}
  return HttpResponseRedirect('/shopicart/home/?msg=Products purchased successfully')
  #return render(request,'shopicart/amountpaid.html',context)

#@login_required
#def history(request,user_id):
  #msg = ''
  #products = History.objects.filter(user_id=int(user_id))  
  #if request.GET.get('msg'):
    #msg = request.GET.get('msg') 
  #if not products:
    #msg = 'No item in history'
  #context = {'message':msg,'products':products,'media':'/media/','username':request.session.get('username'),'userid':request.session.get('userid')}
  #return render(request,'shopicart/historyc.html',context)

@login_required
def history(request,user_id):
  msg = ''
  date = []
  total =0
  msg = 'History'
  products = History.objects.filter(user_id=int(user_id))  
  if request.GET.get('msg'):
    msg = request.GET.get('msg') 
  if not products:
    msg = 'No item in history'
  for product in products:
    date.append(product.pur_date)
    total = product.price + total
  date = list(set(date))
  context = {'total':total,'dates':date,'message':msg,'products':products,'media':'/media/','username':request.session.get('username'),'userid':request.session.get('userid')}
  return render(request,'shopicart/historyc.html',context)

"""
@login_required
def removehistory(request,user_id ,product_id):
  prod_obj = History.objects.get(id=product_id)
  msg = prod_obj.title+' remove from history'
  prod_obj.delete()
  return HttpResponseRedirect('/shopicart/home/userCart/'+user_id+'/history/?msg='+msg)


@login_required
def removehistoryall(request,user_id):
  productid = History.objects.filter(user_id=int(user_id))
  for pid in productid:
    pid.delete()
  context = {'userid':user_id}
  return HttpResponseRedirect('/shopicart/home/userCart/'+user_id+'/history/')
"""

@login_required
def search(request):
  search = request.POST.get('search')
  if search == '':
    return HttpResponseRedirect('/shopicart/home/')
  else:
    return HttpResponseRedirect('/shopicart/home/?search='+search)

@login_required
def historydel(request,user_id,year,month,day):
  prod_obj = History.objects.filter(user_id=user_id)
  for obj in prod_obj:
    if obj.pur_date.day == int(day) and obj.pur_date.month == int(month) and obj.pur_date.year == int(year):
      print obj.delete()
  msg = 'History removed of date :'+day+'/'+month+'/'+year
  return HttpResponseRedirect('/shopicart/home/userCart/'+user_id+'/history/?msg='+msg)