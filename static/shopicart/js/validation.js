
 function checkSpace(str){
  var total=0;
  for (var i=1;i<=str.length;i++){
     if(str[i] == " "){
      total++;
     }
  }
   return total;
}

 function validateFormLogin(id,pass) {
    var temp = 0;
    var x = id.trim();
    var atpos = x.indexOf("@");
    var dotpos = x.lastIndexOf(".");
    if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length) {
      alert("Not a valid e-mail address");
      temp=1;
      return false;
    }

    if(id == "" || id == null){
    	alert("Enter Login ID...");
      temp = 1;
    	return false;
    }

    if( pass == "" || pass == null){
    	alert("Enter password...");
      temp = 1;
    	return false;
    }
    
    if (temp==0) {
      return true;
    }
    else{
      return false;
    }
 }


function validateFormRegister(name,id,pass,cpass) {    
    if(name == "" || id == null || checkSpace(name) > 1){
      alert("Enter Username...");
      return false;
    } 
    
    if(validateFormLogin(id,pass)){
      if(cpass == "" || cpass == null ){
      alert("Enter Confirm Password...");
      return false;
    }

    if(pass != cpass){
      alert("Password is not matched");
      return false;
    }
    }
    else{
      return false
    }
}

